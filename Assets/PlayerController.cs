﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public int HP;
    public int defensePoints;
    public int powerPoints;
    public Animator animate;
	// Use this for initialization
	void Start () {
        HP = 1000;
        defensePoints = 0;
        powerPoints = 0;
        animate = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        Text hpBar = GameObject.Find("PHP").GetComponent<Text>();
        hpBar.text = HP.ToString();
        Text powerBar = GameObject.Find("Ppower").GetComponent<Text>();
        powerBar.text = powerPoints.ToString();
        Text defenseBar = GameObject.Find("Pdefense").GetComponent<Text>();
        defenseBar.text = defensePoints.ToString();
        animate.SetInteger("HP", HP);
	}
    public bool isInAttackState()
    {

        Debug.Log(animate.GetCurrentAnimatorClipInfo(0)[0].clip.name);
        return  (animate.GetCurrentAnimatorClipInfo(0)[0].clip.name == "JUMP00");
       
    }
    public void powerUp()
    {
        animate.Play("Power");
        powerPoints += 30;
    }
    public void defense()
    {
        animate.Play("Defense");
        defensePoints += 30;
    }
    public void attack(ref AIController aiController)
    {
        animate.Play("Attack");
        aiController.takeDamage(100 + powerPoints);
    }
    public void takeDamage(int damagePoints)
    {
        int actualDamage = damagePoints - defensePoints;
        if (actualDamage > 0)
        {
            if (actualDamage > HP)
                HP = 0;
            else
            HP -= actualDamage;
        }
    }
    public bool isDead()
    {
            return HP == 0;
    }
    
}
