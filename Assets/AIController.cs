﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AIController : MonoBehaviour {
    public int HP;
    private PlayerController playerController;
	public Animator animate;
    public int defensePoints;
    public int powerPoints;

    public enum State
    {
        Neutral,
        Defensing,
        Attacking,
        PowerUping
    }

    State state = State.Neutral;
	// Use this for initialization
	void Start () {
             
	}
	void Awake()
	{
		animate = GetComponent<Animator> ();
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
		HP = 1000; 
	}
	
	// Update is called once per frame
	void Update () {
        Text hpBar = GameObject.Find("AHP").GetComponent<Text>();
        hpBar.text = HP.ToString();
        Text powerBar = GameObject.Find("Apower").GetComponent<Text>();
        powerBar.text = powerPoints.ToString();
        Text defenseBar = GameObject.Find("Adefense").GetComponent<Text>();
        defenseBar.text = defensePoints.ToString();

		animate.SetInteger ("HP", HP);
	}
	public bool isInAttackState()
	{
        return (animate.GetCurrentAnimatorClipInfo(0)[0].clip.name == "Attack");
	}
    public bool isInDamageState()
    {
        return (animate.GetCurrentAnimatorClipInfo(0)[0].clip.name == "Damage");
    }
    private void attack()
    {
        animate.Play("Attack");
        playerController.takeDamage(100 + powerPoints);
    }

    public void powerUp()
    {
        animate.Play("PowerUp");
        powerPoints += 30;
    }
    public void defense()
    {
        defensePoints += 30;
        animate.Play("Defense");
    }
    private void doRandomAction()
    {
        System.Random rnd = new System.Random();
        switch (rnd.Next(1, 5))
        {
            case 1:
                defense();
                break;
            case 2:
                attack();
                break;
            case 3:
                powerUp();
                break;
            case 4:
                attack();
                break;
        }
    }

 
    public void performAction()
    {
        Debug.Log(state.ToString());
        switch (state)
        {
            case State.Neutral:
                doRandomAction();

                if (playerController.HP <= HP - 300 || HP + 200 < playerController.HP)
                    state = State.Attacking;

                else if (playerController.powerPoints >= defensePoints + 60)
                    state = State.Defensing;

                else if (playerController.defensePoints >= powerPoints + 60)
                    state = State.PowerUping;

                break;
            case State.Attacking:
                attack();

                if (playerController.powerPoints - defensePoints >= defensePoints + 30)
                    state = State.Defensing;
                else if (playerController.defensePoints - powerPoints >= powerPoints + 30)
                    state = State.PowerUping;

                break;
            case State.Defensing:
                defense();

                if (playerController.powerPoints - defensePoints < powerPoints - playerController.defensePoints || HP + 200 < playerController.HP)
                    state = State.Attacking;
                break;
            case State.PowerUping:
                powerUp();

                if (playerController.defensePoints - powerPoints < powerPoints || HP + 200 < playerController.HP)
                    state = State.Attacking;
                break;
        }
       // attack();
    }
    public void takeDamage(int damagePoints)
    {
        int actualDamage = damagePoints - defensePoints;
        if (actualDamage > 0)
        {
            if (actualDamage > HP)
                HP = 0;
            else
            HP -= actualDamage;
        }
    }
    public bool isDead()
    {
        return HP == 0;
    }
}
