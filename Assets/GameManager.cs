﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	// Use this for initialization
    public enum Turn
    {
        Player,
        AI
    }

    Turn turn = Turn.Player;
    private PlayerController playerController;
    private AIController aiController;

	void Start () {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        aiController = GameObject.Find("AI").GetComponent<AIController>();

	}
	
	// Update is called once per frame
	void Update () {
        Button attackButton = GameObject.Find("AttackButton").GetComponent<Button>();
        Button defenseButton = GameObject.Find("DefenseButton").GetComponent<Button>();
        Button powerButton = GameObject.Find("PowerButton").GetComponent<Button>();
        switch (turn)
        {
		case Turn.Player:
           if (playerController.isDead())
                {
                    attackButton.interactable = false;
                    defenseButton.interactable = false;
                    powerButton.interactable = false;
                }
           else if (!aiController.isInAttackState()) 
                {
				    attackButton.interactable = true;
				    defenseButton.interactable = true;
                    powerButton.interactable = true;
			    }
            break;

        case Turn.AI:
            if (aiController.isDead())
            {
                attackButton.interactable = false;
                defenseButton.interactable = false;
                powerButton.interactable = false;
            } else if (!playerController.isInAttackState())
                { 
                    aiController.performAction();
                    turn = Turn.Player;
                }

            attackButton.interactable = false;
            defenseButton.interactable = false;
            powerButton.interactable = false;
            break;
        }
              
	}

    public void handleAttackButton()
    {
        playerController.attack(ref aiController);     
        turn = Turn.AI;
    }
    public void handleDefenseButton()
    {
        playerController.defense();
        turn = Turn.AI;
    }

    public void handlePowerUpButton()
    {
        playerController.powerUp();
        turn = Turn.AI;
    }

}
